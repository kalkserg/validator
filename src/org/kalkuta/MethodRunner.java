package org.kalkuta;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class MethodRunner {

    /**
     * @param clazz      класс объекта на котором нужно вызвать метод
     * @param obj        обьект класса, или null если метод статический
     * @param methodName имя метода
     * @param paramTypes массив типов параметров метода
     * @param params     сами параметры
     * @return возвращаемое методом значение
     */
    public static Object invoke(Class<?> clazz, Object obj, String methodName, Class<?>[] paramTypes, Object... params) {

        Method method = null;

        try {
            //method.setAccessible(true);
            method = clazz.getDeclaredMethod(methodName, paramTypes);
            method.setAccessible(true);
            Parameter[] parameters = method.getParameters();
            int i = 0;
            for (Parameter parameter : parameters) { // можно просто пройтись циклом foreach по Parameter[]
               if(parameter.isAnnotationPresent(Valid.class)){
                   Validation v = new Validation();
                   v.validate(params[i++]);
               }
            }
        } catch (NoSuchMethodException | ValidationException e) { // для блоков catch с одинаковой логикой можно использовать синтаксис catch (EX1 | EX2 | EX3 e)
            e.printStackTrace();
        }

        // Слишком много блоков try-catch с одинаковой логикой. Оберни весь код в единственный try-catch по Exception
        try {
            Object returnedValue = method.invoke(obj,params); // если в предыдущем блоке трай кетч будет NoSuchMethodException то здесь будет NPE
            return returnedValue;
        } catch (Exception e) {
            System.err.println("Exception " + e);
        }
        return null;
    }

}
