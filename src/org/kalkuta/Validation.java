package org.kalkuta;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Validation implements Validator {

    @Override
    public void validate(Object o) throws ValidationException {
        Field[] fields = o.getClass().getDeclaredFields();

        for (Field field : fields) { // можно использовать цикл foreach
            if(field.isAnnotationPresent(NotNull.class)){ // опять таки можно не перебирать все аннотации подряд у поля есть другие более подходящие методы
                field.setAccessible(true);
                Object value = null;
                try {
                    value = field.get(o);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (value == null) {
                    //System.out.println(field.getDeclaredAnnotation().message()); // сообщение нужно установить в ValidationException
                    throw new ValidationException(field.getAnnotation(NotNull.class).message());
                }
            }
        }
    }


    @Override
    public Class getAnnotationType() {
        return null;
    }
}
