package org.kalkuta;

public class Main {

    public static void main(String[] args) throws ValidationException {
        Main m = new Main();
        //Pojo pojo = new Pojo("aaa");
        Pojo pojo = new Pojo(null);
        Object print = MethodRunner.invoke(Main.class, m, "print", new Class<?>[]{Pojo.class}, pojo);
    }

    private void print (@Valid Pojo p) {
        System.out.println(p);
    }

}

