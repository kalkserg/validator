package org.kalkuta;

import java.lang.annotation.Annotation;

public interface Validator<T extends Annotation> {
    void validate(Object o) throws ValidationException;

    Class<T> getAnnotationType();
}
